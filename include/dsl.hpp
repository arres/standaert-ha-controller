// Copyright (C) Roel Standaert
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "button_event.hpp"

namespace StandaertHA::DSL {

class Event;

class Context {
private:
  const ButtonEvent &currentEvent_;
  const uint32_t outputBefore_;
  uint32_t outputAfter_;

public:
  constexpr Context(const ButtonEvent &currentEvent,
                    uint32_t outputBefore);

  constexpr uint32_t outputAfter() const { return outputAfter_; }

  inline constexpr Event on(ButtonEvent onEvent);
  inline constexpr Event onPressStart(uint8_t button);
  inline constexpr Event onPressEnd(uint8_t button);

private:
  friend class Event;
};

class Event {
private:
  Context &ctx_;
  ButtonEvent onEvent_;

public:
  constexpr inline Event toggle(uint8_t output) &&;
  constexpr inline Event turnOn(uint8_t output) &&;
  constexpr inline Event switchOff(uint8_t output) &&;
  
private:
  constexpr Event(Context &ctx,
                  ButtonEvent onEvent);

  friend class Context;
};

}

#include "dsl.ipp"
